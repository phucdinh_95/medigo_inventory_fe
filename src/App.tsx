import React from 'react';

const App: React.FC = React.memo(() => {

    return (
        <h1>Hello Medigo</h1>
    );
});

export default App;
