export const setBodyClassName = (
    className: string
) => {
    document.body.className = className;
};
